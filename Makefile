# Makefile

CC = gcc
CFLAGS = -Wall #-O3

CSRC = main.c agent.c helpers.c item.c astar.c inventory.c pipe.c map.c view.c bfs.c

HSRC = pipe.h
OBJ1 = $(CSRC:.c=.o)

%o:%c $(HSRC)
	$(CC) $(CFLAGS) -c $<

# additional targets
#.PHONY: clean

agent: $(OBJ1)
	$(CC) -lm $(CFLAGS) -o agent $(OBJ1)
	rm -f *.o

clean:
	rm -f *.o agent
