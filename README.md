# README #

### Quick Start

 1. Clone repository using `git clone https://bitbucket.org/ldavies/3411_a3.git`
 2. Change directory `cd 3411_a3` and make using `make`.
 3. In one terminal start the server using `./server.sh` and in another terminal start the client using `./client.sh`.
 4. Watch the agent solve the maze!
 5. To change the maze being used, edit server.sh and set the maze file to one of s0.in to s8.in or your own maze file!