/*********************************************
 *  agent.c
 *  Agent for Text-Based Adventure Game
 *  Laurence Davies z3342909
 *  COMP3411 Artificial Intelligence
 *  UNSW Session 1, 2013
*/

#include "agent.h"
#include "constants.h"
#include "map.h"
#include "item.h"
#include "inventory.h"

#include <stdio.h>
#include <stdlib.h>


int init_agent(agent_state * a) {
  a->p.i = ORIGIN_OFFSET;
  a->p.j = ORIGIN_OFFSET;
  a->r = 0;
  a->traversed_length = 0;

  init_inventory(&(a->inv[0]));
  init_inventory(&(a->inv[1]));
  init_inventory(&(a->inv[2]));
  init_inventory(&(a->inv[3]));

  return 1;
}

/* Deep copy the agent inventories */
int duplicate_agent(agent_state * src, agent_state * dest) {
  int i;
  int res=1;
  if (!src || !dest) return 0;
  dest->p = src->p;
  dest->r = src->r;
  dest->move = src->move;
  dest->traversed_length = src->traversed_length;
  for (i=0;i<dest->traversed_length;i++) dest->traversed[i]=src->traversed[i];
  
  for (i=0;i<MAXINVENTORIES;i++) res &= deep_copy_inventory(&src->inv[i],&dest->inv[i]);
#ifdef DEBUG
  //if (res) printf("Duplicate agent succeeded.\n");
  if(!res) printf("Duplicate agent failed.\n");
#endif
  return res;
}

/*
int erase_agent_inventories(agent_state * a) {
  int i;
  int res=1;
  for (i=0;i<MAXINVENTORIES;i++) res &= erase_inventory(a->inv[i]);
  return res;
}
*/

int was_traversed(agent_state * a, posn p) {
  int i;
  for (i=0;i<a->traversed_length;++i) if (a->traversed[i].i == p.i && a->traversed[i].j == p.j) return 1;
  return 0;
}

/* Traverse the current position for the agent state */
int traverse(agent_state * a) {
  a->traversed[a->traversed_length++] = a->p;
  return 1;
}

/* Maps an item as represented in the view e.g. 'g' to the respective inventory for an agent_state */
inventory * get_inventory(agent_state * a, char type) {
  switch (type) {
    case GOLD:     return &(a->inv[0]);
    case DYNAMITE: return &(a->inv[1]);
    case KEY:      return &(a->inv[2]);
    case AXE:      return &(a->inv[3]);
    default: 
    printf("Error: item %c is unidentifiable", type); 
    exit(0);
  }
}

/* Removes the item at position [i,j] from the inventory.seen
 * and adds +1 to the inventory.has
 */
int acquire (agent_state * a, char type, posn p) {
  /* Search through inventory and remove it if present */
  inventory * inv = get_inventory(a,type);
  item * k = inv->seen;
  int match = 0;
  while (k != NULL && !match) {
    if (k->p.i == p.i && k->p.j == p.j) match = 1;
    else k = k->next;
  }
  if (match) {
    /* Remove the item from the seen list. Effectively kills the memory at k. Potential leak for large worlds that don't finish. */
    if (k == inv->seen) {
      /* The first item is the match.
       * So clear this item and assign the next as the first */
      inv->seen = k->next;
      if (inv->seen) inv->seen->previous = NULL;
      clear_item(k);
    }
    else if (k->previous != NULL) {
      item * prev = k->previous;
      item * next = k->next;
      prev->next = next;
      if (next != NULL) next->previous = prev;
      clear_item(k);
    }
  }
  /* Add the item to the inventory.has regardless of match */
  inv->has++;
  inv->num_seen--;

  return 1;
}
  

/* Pushes item onto agent inventory list based on type 
 * Fairly dumb. Doesn't check whether item is already in 
 * inventory at position [i,j] 
 */
int agent_sight(agent_state * a, char type, posn p) {
  inventory * inv = get_inventory(a,type);
  return sight(inv,p);
/*
  if (inv->seen != NULL) {
    item * i = inv->seen;
    while (i->next != NULL) {
      // Check if we have seen an item at this position before
      if (i->p.i == p.i && i->p.j == p.j) return 0;
      i = i->next;
    }
    if (i->p.i == p.i && i->p.j == p.j) return 0;

    item * k = &(inv->heap[inv->heap_next++]);
    *k = init_item(p);
    k->next = NULL;

    i->next = k;
    k->previous = i;
  }  
  else {
    item * k = &(inv->heap[inv->heap_next++]);
    *k = init_item(p);
    k->next = NULL;

    inv->seen = k;
    k->previous = NULL;
  }
  inv->num_seen++;

  return 1;
*/
}

char get_agent_action(agent_state * as, maps * map, int allow_unvisited, int can_use_dynamite) {
  inventory * inv;

  /* First check if we have visited this position. If so, it will be whitespace 
   * in the "current" map state. This method saves on duplicating the map 
   * for each agent state, thus conserving memory.
   */
  if (was_traversed(as,as->p)) {
      return ACTION_MOVE_TO;
  }


  switch(map_visible(map,as->p)) {

    case UNVISITED:
      if (allow_unvisited == BLOCK_UNVISITED) return 0;
      return ACTION_MOVE_TO;

    case WATER:
      return 0;

    case TREE: 
      inv = get_inventory(as,AXE);
      if (!inv->has) return 0;
      return ACTION_CHOP_AND_MOVE_TO; 

    case DOOR: 
      inv = get_inventory(as,KEY);
      if (!inv->has) return 0;
      return ACTION_OPEN_AND_MOVE_TO;

    case WALL:
      inv = get_inventory(as,DYNAMITE);
      if (!inv->has || !can_use_dynamite) return 0;
      inv->has--; 
      inv->num_used++; 
      traverse(as);
      return ACTION_BLAST_AND_MOVE_TO;

    case GOLD: 
    case DYNAMITE: 
    case KEY: 
    case AXE:
      acquire(as,map_visible(map,as->p),as->p);
      traverse(as);
      return ACTION_MOVE_TO;

    default:
      return ACTION_MOVE_TO; /* Typically arrive here when agent encounters WHITESPACE */

  }
}

/* return number of moves appended to sequence */
int translate_action_move_sequence(agent_state * seq, char action, agent_state * initial, agent_state * next) {
  if (ACTION_INVALID(action)) return 0;
  int num_moves = 0;
  agent_state cur_agent;
  duplicate_agent(initial,&cur_agent);

  int rotate = next->r - initial->r;
  switch (rotate) {
    case -3: 
      rotate = 1;
      break;

    case 3:
      rotate = -1;
      break;
  }
  /* Turn towards the search direction. */
  while (rotate) {
    if (rotate > 0) {
      rotate--;
      // TODO later make r consistent 
      cur_agent.r = (cur_agent.r + 1) % 4;
      cur_agent.move = RIGHT;
    }
    else {
      rotate++;
      // TODO later make r consistent // seq->r  = ?
      cur_agent.r = (cur_agent.r - 1+4) % 4;
      cur_agent.move = LEFT;
    }
    duplicate_agent(&cur_agent,seq);
    seq++;
    num_moves++;
  }

  switch (action) {
    case ACTION_CHOP_AND_MOVE_TO:
      duplicate_agent(&cur_agent,seq);
      seq->move = CHOP;
      seq++;
      duplicate_agent(next,seq);
      seq->move = FORWARDS;
      seq++;
      num_moves+=2;
      break;
    case ACTION_BLAST_AND_MOVE_TO:
      duplicate_agent(&cur_agent,seq);
      seq->move = BLAST;
      seq++;
      duplicate_agent(next,seq);
      seq->move = FORWARDS;
      seq++;
      num_moves+=2;
      break;
    case ACTION_OPEN_AND_MOVE_TO:
      duplicate_agent(&cur_agent,seq);
      seq->move = OPEN;
      seq++;
      duplicate_agent(next,seq);
      seq->move = FORWARDS;
      seq++;
      num_moves+=2;
      break;
    case ACTION_MOVE_TO:
      duplicate_agent(next,seq);
      seq->move = FORWARDS;
      seq++;
      num_moves++;
      break;
  }
  return num_moves;
}

/* Returns 1 if the inventories are the same between agents, 0 otherwise */
int compare_agent_inventories(agent_state * as1, agent_state * as2) {
  int i;
  int res = 1;
  for (i=0;i<MAXINVENTORIES;i++) {
    int res1 = compare_inventory(&as1->inv[i],&as2->inv[i]);
    res = res && res1;
  }
  return res;
}

/* Returns 1 if the inventories HAVE improved from as1 to as2, 0 otherwise */
/* One last check is required for agent state: Compare the traversed path.
 * Only need to store and thus compare traversed paths where the map state will have
 * changed from whatever item or wall or tree to blank. */
int have_agent_inventories_improved(agent_state * as1, agent_state * as2) {
  int i;
  int res = 0;
  for (i=0;i<MAXINVENTORIES;i++) {
    int res1 = has_inventory_improved(&as1->inv[i],&as2->inv[i]);
    res = res || res1;
  }
  return res;
}

int compare_agent_posn(agent_state * as1, agent_state *as2) {
  return as1->p.i == as2->p.i && as1->p.j == as2->p.j;
}


/* Return 1 if all positions in traverse of as1 are in traverse of as2, 0 otherwise */
int compare_agent_traverse(agent_state * as1, agent_state * as2) {
  int i;
  for (i=0;i<as1->traversed_length;++i)
    if (!was_traversed(as2,as1->traversed[i])) return 0;
  return 1;
}

int compare_agent_state(agent_state * as1, agent_state *as2) {
  return compare_agent_posn(as1,as2) && compare_agent_inventories(as1,as2);
}

