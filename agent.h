/*********************************************
 *  agent.c
 *  Agent for Text-Based Adventure Game
 *  Laurence Davies z3342909
 *  COMP3411 Artificial Intelligence
 *  UNSW Session 1, 2013
*/

#ifndef AGENT_H
#define AGENT_H

#include "structs.h"


int init_agent(agent_state * a);

int duplicate_agent(agent_state * src, agent_state * dest);

int was_traversed(agent_state * a, posn p);

/* Maps an item as represented in the view e.g. 'g' to the respective inventory for an agent_state */
inventory * get_inventory(agent_state * a, char type);

/* Removes the item at position [i,j] from the inventory.seen
 * and adds +1 to the inventory.has
 */
int acquire (agent_state * a, char type, posn p);

/* Pushes item onto agent inventory list based on type 
 * Fairly dumb. Doesn't check whether item is already in 
 * inventory at position [i,j] 
 */
int agent_sight(agent_state * a, char type, posn p);

//char check_agent_state(agent_state * as, maps * map, int allow_unvisited);
char get_agent_action(agent_state * as, maps * map, int allow_unvisited, int can_use_dynamite);


int compare_agent_inventories(agent_state * as1, agent_state * as2);
int compare_agent_posn(agent_state * as1, agent_state *as2);
int compare_agent_state(agent_state * as1, agent_state *as2);

/* Return 1 if all positions in traverse of as1 are in traverse of as2, 0 otherwise */
int compare_agent_traverse(agent_state * as1, agent_state * as2);

/* Returns 1 if the inventories HAVE improved between agents, 0 otherwise */
int have_agent_inventories_improved(agent_state * as1, agent_state * as2);

//int translate_action_move_sequence(char * seq, char action, int initial_r, int final_r);
int translate_action_move_sequence(agent_state * seq, char action, agent_state * initial, agent_state * next);

#endif
