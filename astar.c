/*********************************************
 *  agent.c
 *  Sample Agent for Text-Based Adventure Game
 *  COMP3411 Artificial Intelligence
 *  UNSW Session 1, 2013
*/


#include "astar.h"
#include "constants.h"
#include "map.h"
#include "view.h"
#include "agent.h"
#include "astar.h"
#include "bfs.h"

#include <stdio.h>
#include <stdlib.h>



int init_queue (queue * q) {
  q->first_open   = NULL;
  q->first_closed = NULL;
  q->heap_next    = 0;

  return 1; /* Success? */
}

queue_node * alloc_next_node (queue * q, agent_state * as) {
  if (!q || !as) return NULL; /* FIXME use a better sentinel for fail conditions */

  if (q->heap_next >= MAXQUEUESIZE-1) {
    printf("Queue size exceeded. Exiting...\n");
    exit(0);
  }
  queue_node * n = &(q->heap[q->heap_next++]);
  /* Initialise */
  n->cost          = MAXQUEUESIZE;
  n->depth         = 0;
  n->parent        = NULL;
  n->child         = NULL;
  n->next_move     = NULL;
  n->previous_move = NULL;
  n->action        = 0;
  duplicate_agent(as, &n->as); /* Copy the agent state, TODO catch sentinel */
  
  return n;
}

/* Enqueues by n.cost. q.first has lowest cost */
/* TODO as we pass each node, compare critical factors 
 * i.e. same inventory and posn
 * If the comparison is true, don't enqueue the node.
 */
int enqueue_open (queue * q, queue_node * n) {
  if (q->first_open == NULL) {
    q->first_open = n;
    n->parent = NULL;
    n->child = NULL;
  }
  else {
    queue_node * c = q->first_open;
    /* if n has a lower cost than all in the queue,
     *  enqueue it first. */
    if (n->cost < c->cost) {
      q->first_open = n;
      n->parent = NULL;
      n->child = c;
      c->parent = n;
      /* c->child remains the same */
    }
    else {
      while (c->child != NULL && c->child->cost < n->cost) {
        /* As we pass each node in the queue, compare critical factors 
         * i.e. same inventory and posn
         * If the comparison is true, don't enqueue the node.
         * FIXME 21/6/2015 this comparison is invalid because of the changing map state. Fix this.
         */
#if 0
        if (compare_agent_state(&c->as,&n->as)) {
          /* Don't enqueue node. Break early. */
#if 0
  {
    inventory * gold = get_inventory(&n->as,GOLD);
    inventory * dyn  = get_inventory(&n->as,DYNAMITE);
    inventory * axe  = get_inventory(&n->as,AXE);
    inventory * key  = get_inventory(&n->as,KEY);
    printf("NOT ENQUEUEING n.depth=%d n.cost=%d n.as.p=(%d,%d), n.as.r=%d, n.gold.has=%d n.dyn.has=%d n.axe.has=%d n.key.has=%d q.size=%d\n",
            n->depth,n->cost,n->as.p.i,n->as.p.j,n->as.r,
            gold->has, dyn->has, axe->has, key->has,
            q->heap_next
        );
  }
#endif
          return 0;
        }
        else
#endif
            c = c->child;
      }
      n->child = c->child;
      n->parent = c;
      c->child = n;
    }
  }
#if 0
  {
    inventory * gold = get_inventory(&n->as,GOLD);
    inventory * dyn  = get_inventory(&n->as,DYNAMITE);
    inventory * axe  = get_inventory(&n->as,AXE);
    inventory * key  = get_inventory(&n->as,KEY);
    printf("Enqueue open n.depth=%d n.cost=%d n.as.p=(%d,%d), n.as.r=%d, n.gold.has=%d n.dyn.has=%d n.axe.has=%d n.key.has=%d q.size=%d\n",n->depth,n->cost,n->as.p.i,n->as.p.j,n->as.r,
            gold->has, dyn->has, axe->has, key->has,
            q->heap_next
        );
  }
#endif
  return 1; 
}

/* Adds to the end of the closed queue. */
int enqueue_closed (queue * q, queue_node * n) {
  queue_node * c = q->first_closed;
  if (c == NULL) q->first_closed = n;
  else {
    while (c->child != NULL) {
      c = c->child;
    }
    c->child = n;
  }
  n->parent = c;
  n->child = NULL;
  return 1; 
}

/* Return 1 if the node is in the closed set. Compare agent state only. */
int is_node_closed(queue * q, queue_node * n) {
  queue_node * c = q->first_closed;
  if (c == NULL) return 0;
  else {
    do {
      //if (/*c->cost <= n->cost &&*/ compare_agent_state(&c->as,&n->as)) return 1;
      //if (compare_agent_posn(&c->as,&n->as) && !have_agent_inventories_improved(&c->as,&n->as) && compare_agent_traverse(&n->as, &c->as)) return 1;
      if (compare_agent_posn(&c->as,&n->as) && compare_agent_state(&c->as,&n->as) && compare_agent_traverse(&n->as, &c->as)) return 1;
      c = c->child;
    }
    while (c != NULL);
  }
  return 0; 
}

/* Return 1 if the node is in the open set. Compare agent state only. */
int is_node_open(queue * q, queue_node * n) {
  queue_node * c = q->first_open;
  if (c == NULL) return 0;
  else {
    do {
      //if (/*c->cost <= n->cost &&*/ compare_agent_state(&c->as,&n->as)) return 1;
      //if (compare_agent_posn(&c->as,&n->as) && !have_agent_inventories_improved(&c->as,&n->as) && compare_agent_traverse(&n->as, &c->as)) return 1;
      if (compare_agent_posn(&c->as,&n->as) && compare_agent_state(&c->as,&n->as) && compare_agent_traverse(&n->as, &c->as)) return 1;
      c = c->child;
    }
    while (c != NULL);
  }
  return 0; 
}

/* Dequeue the lowest cost node */
queue_node * pop_first (queue * q) {
  queue_node * f = q->first_open;
  if (q->first_open != NULL) q->first_open = q->first_open->child;
  if (q->first_open != NULL) q->first_open->parent = NULL;
  return f;
}
  
/* Assumes that n is not the root of the result path */
int chase_search_path (a_star_search_result * s, queue_node * n) {
  // TODO check args, return 0 sentinel on fail.
  //a_star_search_result s = {.found = 1, .length = 0, .cost = n->cost, .p = n->as.p};
  s->found = 1;
  s->length = 0;
  s->cost = n->cost;
  // don't do s->p, because we push the agent_state now.
  /* Traverse backwards to start of path */
  while (n != NULL && n->previous_move != NULL) {
    n->previous_move->next_move = n;
    n = n->previous_move;
  }
  /* For each step forwards, construct a sequence of moves.
   * Check the next_move is not null because the final "move" 
   * is the last position. */
  while (n != NULL && n->next_move != NULL) {
    queue_node * next = n->next_move;
    s->length += translate_action_move_sequence(&s->moves[s->length],next->action,&n->as,&next->as);
    n = next;
  }
  //s.r = n->as.r;
  return 1;
}


int a_star_gold( a_star_search_result * asresult, agent_state  * as, maps * map ) {
  inventory * gold = get_inventory(as,GOLD);
  /* for each gold that has been seen */
  item * g = gold->seen;
  while (g != NULL && !asresult->found) {

#ifdef DEBUG
printf("Start A* gold search\n");
printf("At i=%d j=%d\n",g->p.i,g->p.j);
#endif

      int success = a_star_base( asresult, as, map, GOLD_SEARCH_TYPE, g->p, 1);

      debug_print_result(asresult,"gold",as,map);

      if (!success) {
#ifdef DEBUG
        printf("Failed gold search.\n");
#endif
      }
      else return success;
    g = g->next;
  }
  return 0;
  //a_star_base( asresult, as, map, GOLD_SEARCH_TYPE, init_posn(80,80), 1);
}


int a_star_origin( a_star_search_result * asresult, agent_state  * as, maps * map ) {
  posn origin = {.i = ORIGIN_OFFSET, .j = ORIGIN_OFFSET};
  int success = a_star_base( asresult, as, map, TARGET_SEARCH_TYPE, origin, 1);
  debug_print_result(asresult,"origin",as, map);
  if (!success) {
    printf("Failed return to origin search.\n");
  }
  return success;
}

int a_star_unvisited( a_star_search_result * asresult, agent_state  * as, maps * map ) {
  posn origin = {.i = ORIGIN_OFFSET, .j = ORIGIN_OFFSET};
  int success = a_star_base( asresult, as, map, UNVISITED_SEARCH_TYPE, origin, 1);
  debug_print_result(asresult,"unvisited",as, map);
  if (!success) {
    printf("Failed unvisited search.\n");
  }
  return success;
}

int a_star_unvisited_nodynamite( a_star_search_result * asresult, agent_state  * as, maps * map ) {
  posn origin = {.i = ORIGIN_OFFSET, .j = ORIGIN_OFFSET};
  int success = a_star_base( asresult, as, map, UNVISITED_NODYNAMITE_SEARCH_TYPE, origin, 1);
  debug_print_result(asresult,"unvisited no-dynamite",as, map);
  if (!success) {
    printf("Failed unvisited no-dynamite search.\n");
  }
  return success;
}


/* Uses BFS plus manhatten distance cost heuristic to order enqueued search nodes */
int a_star_base( 
        a_star_search_result * asresult,
        agent_state  * as, 
        maps * map, 
        int search_type,
        posn target, 
        int allow_suboptimal) 
{
  int r; 
  int max_depth = get_uncovered(map)/2;
  queue * q = malloc(sizeof(queue)); /* there is not enough stack space for a queue */
  if (!q) {
#ifdef DEBUG
    printf("Error: Could not allocate memory on heap for search queue.\n ");
    exit(-1);
#endif
  }
  init_queue(q);

  init_cost_path(map);

  /* start with first node */
  queue_node * d = alloc_next_node(q, as);
  set_cost_path(d->as.p, map, 0);
  enqueue_open(q,d);

  int iterations = 1;

  /* dequeue best search node from open_set to current. If null, there are no searches left! */
  //while ((d = pop_first(q)) != NULL && iterations < (MAXQUEUESIZE/4)) {
  while ((d = pop_first(q)) != NULL) {
    if (q->heap_next >= MAXQUEUESIZE-4) return 0;

    switch (search_type) {
      case UNVISITED_NODYNAMITE_SEARCH_TYPE:
      case UNVISITED_SEARCH_TYPE:
        /* TODO add unvisited search type here */
        if (viewing_window_has_unvisited(map,d->as.p)) {
    #ifdef DEBUG
          printf("Found unvisited space: at [%d,%d]\n", d->as.p.i,d->as.p.j);
    #endif
          chase_search_path(asresult, d);
          free(q);
          reset_cost_map_view(map);
          return 1;
        } 
        break;

      case GOLD_SEARCH_TYPE:
      case TARGET_SEARCH_TYPE:
      default:
        /* if current is goal, reconstruct the path to the goal
         *  and return the r (inital direction) in search_result 
         */
        if (d->as.p.i == target.i && d->as.p.j == target.j) {
          int res = chase_search_path(asresult, d);
          free(q);
          reset_cost_map_view(map);
          return res;
        } 
        break;
    }

    /* otherwise, remove current from open_set and add it to the closed set */
    enqueue_closed(q, d);

    /* enqeue u,l,d,r search nodes to open_set */
    for (r=0;r<4;r++) {
      agent_state new_as;
      duplicate_agent(&d->as,&new_as);
      new_as.p = move_posn(d->as.p, r);
      new_as.r = r;
      int trav = was_traversed(&new_as,new_as.p); // before getting the action, see if this posn was traversed for debug

      char action = 0;
      unsigned long goal_distance = 0;
      unsigned long cost = 0;
      unsigned long unvisited_cost = 0;
      inventory * key      = get_inventory(&new_as,KEY);
      inventory * axe      = get_inventory(&new_as,AXE);
      inventory * dynamite = get_inventory(&new_as,DYNAMITE);

      switch (search_type) {
        case UNVISITED_SEARCH_TYPE:
          /* get_agent_action returns an action that will be assigned to 
           * a queue_node for a new agent_state that defines the action
           * required to get to that state. */
          action = get_agent_action(&new_as, map, BLOCK_UNVISITED, 1);

          cost = d->depth + 1;
          // another penalty for not having the key or the axe
          if (!key->has) cost += 10; // figure out these numbers later
          if (!axe->has) cost += 20; // figure out these numbers later
          //if (!dynamite->has) cost += 50; // figure out these numbers later
          cost += dynamite->num_seen * 50; // figure out these numbers later
          // Add a cost heuristic for the distance to the closest unvisited space.
          // To find unvisited space, spiral out in concentric squares from agent.
          unvisited_cost = closest_unvisited(map,new_as.p);
          cost += unvisited_cost;
          // if we had to use dynamite to get to this state, add additional cost.
          cost += dynamite->num_used * unvisited_cost;
    
          // impart a penalty for already searched nodes, regardless of inventory.
          //cost += !compare_cost_path(new_as.p,map,cost) * max_depth;
          cost += !compare_cost_path(new_as.p,map,cost) * get_uncovered();

          break;


        case UNVISITED_NODYNAMITE_SEARCH_TYPE:
          /* TODO add remainder of the cost function for unvisited searches */

          /* get_agent_action returns an action that will be assigned to 
           * a queue_node for a new agent_state that defines the action
           * required to get to that state. */
          action = get_agent_action(&new_as, map, BLOCK_UNVISITED, 0);

          cost = d->depth + 1;
          // Add a cost heuristic for the distance to the closest unvisited space.
          // To find unvisited space, spiral out in concentric squares from agent.
          unvisited_cost = closest_unvisited(map,new_as.p);
          cost += unvisited_cost;
    
          // impart a penalty for already searched nodes, regardless of inventory.
          //cost += !compare_cost_path(new_as.p,map,cost) * max_depth;
          cost += !compare_cost_path(new_as.p,map,cost) * get_uncovered();

          break;
    
        case GOLD_SEARCH_TYPE:

          /* get_agent_action returns an action that will be assigned to 
           * a queue_node for a new agent_state that defines the action
           * required to get to that state. */
          action = get_agent_action(&new_as, map, BLOCK_UNVISITED, 1);
    
          // another penalty for not having the key or the axe
          if (key->num_seen && !key->has) cost += 50; // figure out these numbers later
          if (axe->num_seen && !axe->has) cost += 50; // figure out these numbers later
          cost += dynamite->num_seen * 100; // figure out these numbers later
          // if we had to use dynamite to get to this state, add additional cost.
          //if (map->visible[new_as.p.i][new_as.p.j] == WALL) cost += MAXQUEUESIZE;//get_uncovered();
          //cost += dynamite->num_used /** get_uncovered() **/ * 30030;

          /* calc cost of search node */
          goal_distance = abs(new_as.p.i - target.i) + abs(new_as.p.j - target.j);
          cost += d->depth + 1 + goal_distance;

          cost += dynamite->num_used * goal_distance;

          /* Add a tiny distance penalty to each dynamite */
          item * dyn = dynamite->seen;
          while (dyn) {
            unsigned long dcost = abs(new_as.p.i - dyn->p.i) + abs(new_as.p.j - dyn->p.j);
            dcost /= 2; // figure out a better constant weight for this.
            cost += dcost;
            dyn = dyn->next;
          }
    
          break;

        case TARGET_SEARCH_TYPE:
        default:
          /* get_agent_action returns an action that will be assigned to 
           * a queue_node for a new agent_state that defines the action
           * required to get to that state. */
          action = get_agent_action(&new_as, map, BLOCK_UNVISITED, 1);
          /* calc cost of search node */
          goal_distance = abs(new_as.p.i - target.i) + abs(new_as.p.j - target.j);
          cost = d->depth + 1 + goal_distance;
    
      }

      // mock up a pretend queue node
      queue_node ts = {.cost=cost,.depth=d->depth+1,.action=action,.parent=NULL,.child=NULL,.next_move=NULL,.previous_move=d};
      duplicate_agent(&new_as,&ts.as);
      int node_is_closed = is_node_closed(q,&ts);
      int node_is_open = is_node_open(q,&ts);

#ifdef DEBUG
      printf("action: %d ts.depth: %d maxdepth: %d view: %c traversed: %d closed: %d open: %d queue size: %d from: [%d,%d]\n",action, ts.depth, max_depth, map_visible(map,new_as.p),trav,node_is_closed, node_is_open, q->heap_next,d->as.p.i,d->as.p.j);
      debug_print_agent_state(&new_as, map, 0);
      print_cost_map(map, &new_as);
      printf("\x1B[A\x1B[A\x1B[A\x1B[A\x1B[A\x1B[A");
      printf("\x1B[A");
      printf("\x1B[A");
      printf("\x1B[A");
      printf("\x1B[A");
      // temp see what is pushing it down
      //int tp; for (tp=0;tp<map->extent_i_max-map->extent_i_min+10;++tp) printf("\n");
#endif
      
      //if (ACTION_VALID(action)) {
      // FIXME unblocked check may not be optimal
      if (action && 
              //ts.depth < max_depth &&
              /*compare_cost_path(new_as.p,map, cost) &&*/ 
              !node_is_closed &&
              (!node_is_open || !allow_suboptimal)
              ) {
        queue_node * s;
        /* set move after node alloc for now. FIXME refactor. */
        s = alloc_next_node(q, &new_as);

        s->action = action;
        s->previous_move = d;
        s->depth = d->depth + 1;
        /* calc cost of search nodes */
        s->cost = cost;

        if (compare_cost_path(s->as.p,map,s->cost))
          set_cost_path(s->as.p, map, s->cost);

        /* Now enqueue */
        //if (!is_node_closed(q,s)) {
        if (!node_is_closed) {
          if (enqueue_open(q,s))
            set_map_extents(map,s->as.p);

#ifdef DEBUG
          //printf("Enqueued node in a*search.\n");
          //print_map(map,&s->as);
        }
        else {
//          printf("Did not enqueue node in a* search. Already in closed set.\n");
#endif
        }

        iterations++;
      }
    }
  }

  free (q);
  reset_cost_map_view(map);
  return 0;

}

int debug_print_result(a_star_search_result * s, const char * c, agent_state * agent, maps * map) {
#ifdef DEBUG
  printf("Result of %s search map[%d][%d]='%c'\n",c,s->p.i,s->p.j,map_visible(map,s->p));
  printf("Found: %d Length: %d Cost: %lu\n",s->found, s->length, s->cost);
  debug_print_agent_state(agent, map, 0);
#endif
  return 1;
}

int debug_print_agent_state(agent_state * agent, maps * map, int overwrite) {
#ifdef DEBUG
  inventory * inv;
  printf("Map extents i=[%d .. %d] j=[%d .. %d]\n",map->extent_i_min,map->extent_i_max,map->extent_j_min,map->extent_j_max);
  printf("Agent state: seen have\n");
  inv = get_inventory(agent,GOLD);
  printf("       gold:    %d    %d\n",inv->num_seen,inv->has);
  inv = get_inventory(agent,AXE);
  printf("        axe:    %d    %d\n",inv->num_seen,inv->has);
  inv = get_inventory(agent,KEY);
  printf("        key:    %d    %d\n",inv->num_seen,inv->has);
  inv = get_inventory(agent,DYNAMITE);
  printf("   dynamite:    %d    %d\n",inv->num_seen,inv->has);
  if (overwrite) 
    printf("\x1B[A\x1B[A\x1B[A\x1B[A\x1B[A\x1B[A");
#endif
  return 1;
}
