/*********************************************
 *  agent.c
 *  Sample Agent for Text-Based Adventure Game
 *  COMP3411 Artificial Intelligence
 *  UNSW Session 1, 2013
*/

#ifndef ASTAR_H
#define ASTAR_H

#include "structs.h"

int init_queue (queue * q);

queue_node * alloc_next_node (queue * q, agent_state * as);

/* Enqueues by n.cost. q.first has lowest cost */
int enqueue_open (queue * q, queue_node * n);

/* Adds to the end of the closed queue. */
int enqueue_closed (queue * q, queue_node * n);

/* Return 1 if the node is in the closed set. Compare agent state only. */
int is_node_closed(queue * q, queue_node * n);

/* Return 1 if the node is in the open set. Compare agent state only. */
int is_node_open(queue * q, queue_node * n);

/* Dequeue the lowest cost node */
queue_node * pop_first (queue * q);
  
/* Assumes that n is not the root of the result path */
int chase_search_path ( a_star_search_result * asresult,queue_node * n);

/* Uses BFS plus cost heuristic */
int a_star_gold( a_star_search_result * asresult, agent_state  * as, maps * map );
int a_star_origin( a_star_search_result * asresult, agent_state  * as, maps * map );
int a_star_unvisited( a_star_search_result * asresult, agent_state  * as, maps * map );
int a_star_unvisited_nodynamite( a_star_search_result * asresult, agent_state  * as, maps * map );
int a_star_base( 
        a_star_search_result * asresult,
        agent_state  * as, 
        maps * map, 
        int search_type,
        posn target, 
        int allow_suboptimal);

int debug_print_result(a_star_search_result * s, const char * c, agent_state * agent, maps * map);
int debug_print_agent_state(agent_state * agent, maps * map, int overwrite);
#endif
