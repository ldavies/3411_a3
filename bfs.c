/*********************************************
 *  bfs.c
 *  Agent for Text-Based Adventure Game
 *  Laurence Davies z3342909
 *  COMP3411 Artificial Intelligence
 *  UNSW Session 1, 2013
*/


#include "bfs.h"
#include "constants.h"
#include "helpers.h"
#include "map.h"
#include "view.h"
#include "agent.h"
#include "astar.h"

#include <stdio.h>
#include <stdlib.h>


#if 0
/* Use the queue_node.depth variable to hold the allowed depth 
 * to be searched from a node 
 */
int bfs_unvisited( a_star_search_result * asresult, agent_state * as, maps * map) {
/* 
  const a_star_search_result no_result = {
     // .r = 0,
      .length = 0, .cost = 0, .found = 0, .p = as->p};
*/
  int r; 
#ifdef DEBUG
    printf("Attempting to allocate %lu on heap.\n ", sizeof(queue));
#endif
  queue * q = malloc(sizeof(queue));;
  if (!q) {
#ifdef DEBUG
    printf("Error: Could not allocate memory on heap for search queue.\n ");
    exit(-1);
#endif
  }
  init_queue(q);

  init_cost_path(map);

  /* start with first node */
  queue_node * d = alloc_next_node(q, as);
  // FIXME why is the depth and queue size set to max?
  //d->depth = MAXQUEUESIZE; d->cost = MAXQUEUESIZE;
  //d->depth = 0; d->cost = 0;
  enqueue_open(q,d);
  set_cost_path(d->as.p, map, d->cost);

  int iterations = 1;

  int max_depth = 0;

  /* dequeue best search node from open_set to current. 
   * If null, there are no searches left! 
   */
  //while ((d = pop_first(q)) != NULL && iterations < (MAXQUEUESIZE/4)) {
  while ((d = pop_first(q)) != NULL) {
    if (iterations >= MAXQUEUESIZE) return -1;
    /* Process the current node.
     * If the potential viewing window overlaps with unvisited 
     * nodes, reconstruct the path to the goal 
     * and return the r (inital direction) in search_result 
     */
    //if (map->visible[d->as.p.i][d->as.p.j] == UNVISITED) {
    if (viewing_window_has_unvisited(map,d->as.p)) {
#ifdef DEBUG
      printf("Found unvisited space: at [%d,%d]\n", d->as.p.i,d->as.p.j);
#endif
      chase_search_path(asresult, d);
      free(q);
      reset_cost_map_view(map);
      return 1;
    } 

    /* otherwise, remove current from open_set and add it to the closed set */
    enqueue_closed(q, d);

    /* Not sure what the below does. What changes d->depth? */
    //if (d->depth <= 0) continue;
    //cost++;

    /* enqeue u,l,d,r search nodes to open_set */
    for (r=0;r<4;r++) {
      agent_state new_as;
      duplicate_agent(&d->as, &new_as);
      new_as.p = move_posn(d->as.p, r);
      new_as.r = r;
      char action = get_agent_action(&new_as, map, ALLOW_UNVISITED);

      unsigned long cost = d->depth + 1;
      // another penalty for not having the key or the axe
      inventory * key      = get_inventory(&new_as,KEY);
      inventory * axe      = get_inventory(&new_as,AXE);
      inventory * dynamite = get_inventory(&new_as,DYNAMITE);
      if (!key->has) cost += 10; // figure out these numbers later
      if (!axe->has) cost += 20; // figure out these numbers later
      //if (!dynamite->has) cost += 50; // figure out these numbers later
      cost += dynamite->num_seen * 50; // figure out these numbers later
      // if we had to use dynamite to get to this state, add additional cost.
      //if (map->visible[new_as.p.i][new_as.p.j] == WALL) cost += get_uncovered();
      cost += dynamite->num_used * get_uncovered() * 30030;

      // Add a cost heuristic for the distance to the closest unvisited space.
      // To find unvisited space, spiral out in concentric squares from agent.
      cost += closest_unvisited(map,new_as.p);

      // impart a penalty for already searched nodes, regardless of inventory.
      //cost += !compare_cost_path(new_as.p,map,cost) * max_depth;
      cost += !compare_cost_path(new_as.p,map,cost) * get_uncovered();

      // mock up a pretend queue node
      queue_node ts = {.cost=cost,.depth=d->depth+1,.action=action,.parent=NULL,.child=NULL,.next_move=NULL,.previous_move=d};
      duplicate_agent(&new_as,&ts.as);
      int node_is_closed = is_node_closed(q,&ts);
      

#ifdef DEBUG
      //printf("Evaluating action: %d at [%d,%d]\n", action,new_as.p.i,new_as.p.j);
      printf("action: %d compare_cost_path: %d closed: %d queue size: %d uncovered:%d\n",action, compare_cost_path(new_as.p,map,cost), node_is_closed, q->heap_next, get_uncovered());
      print_cost_map(map, &new_as);
      printf("\x1B[A");
#endif

      //if (ACTION_VALID(action)) {
      //if (action && is_unblocked(new_as.p,map)) {
      if (action && /*compare_cost_path(new_as.p,map,cost) &&*/ !node_is_closed) {
        queue_node * s;
        s = alloc_next_node(q, &new_as);
        s->action = action;
        s->previous_move = d;
        s->depth = d->depth+1;

        max_depth = max(max_depth,s->depth);

        //s->cost = cost;
        s->cost = cost;

        //if (compare_cost_path(s->as.p,map,s->cost)) 
        set_cost_path(s->as.p, map, s->cost);

        //if (!is_node_closed(q,s)) {
        if (!node_is_closed) {
          if (enqueue_open(q,s))
            set_map_extents(map,s->as.p);
#ifdef DEBUG
          //printf("Enqueued node in unvisited search. Action: %d\n", s->action);
          //print_map(map,&s->as);
        }
        else {
          //printf("Did not enqueue node in unvisited search. Already in closed set.\n");
#endif
        }

        iterations++;
      }
    }
  }

  free (q);
  reset_cost_map_view(map);
  return 0;
}
#endif
/*
unsigned long closest_unvisited(maps * map, posn p) {
  int i,j;
  unsigned long k = 0;
  // concentric squares spiraling outwards:
  // cost is sum(2^(n-k)*c_i/c_mk) 
  // where 
  //   c_k  = # known positions in ring
  //   c_mk = # positions in ring
  // TODO: Prove that this cost heuristic is sort of admissible,
  //       or at least doesn't cause strange non-linear behavior.
  unsigned long cost = 0;
  // The number of rings n must be the same
  // for same map dimensions regardless of p.
  unsigned long n = max(map->extent_i_max - map->extent_i_min, map->extent_j_max - map->extent_j_min);
  // Often the outer rings will be wholly outside the map extents
  // due to the placement of p, therefore don't compute them.
  int n_support = max(
                     max(p.i - map->extent_i_min, map->extent_i_max - p.i),
                     max(p.j - map->extent_j_min, map->extent_j_max - p.j)
                     );
  for (k=0;k<n_support;k++) {
    // compute max in ring.
    unsigned long c_m = k ? 8 * k : 1;
    unsigned long c_i = 0;
    // any positions outside the map extents count as unvisited.
    for (i=p.i-k;i<=p.i+k;++i) {
      int inc = (i==p.i-k || i==p.i+k) ? 1 : 2*k;
      for (j=p.j-k;j<=p.j+k;j+=inc)
        c_i +=
             (i >= map->extent_i_min &&
              i <= map->extent_i_max &&
              j >= map->extent_j_max &&
              j <= map->extent_j_max &&
              map_visible(map,init_posn(i,j)) != UNVISITED);
    }
    cost += (1 << (n - k)) * c_i / c_m;
  }
  return cost;
}

*/

unsigned long closest_unvisited(maps * map, posn p) {
  // sum visited map positions weighted by inverse of manhatten distance
  // to the agent.
  int i,j;
  unsigned long d;
  unsigned long cost;
   
  // for convenience
  int i_min = map->extent_i_min;
  int i_max = map->extent_i_max;
  int j_min = map->extent_j_min;
  int j_max = map->extent_j_max;

  for( i=i_min; i <= i_max; i++ ) {
    for( j=j_min; j <= j_max; j++ ) {
      if (map_visible(map,init_posn(i,j)) != UNVISITED) {
        d = abs(p.i - i) + abs(p.j - j);
        cost += d ? 30030 / d : 0;
      }
    }
  }
  return cost;
}
