/*********************************************
 *  bfs.c
 *  Agent for Text-Based Adventure Game
 *  Laurence Davies z3342909
 *  COMP3411 Artificial Intelligence
 *  UNSW Session 1, 2013
*/

#ifndef BFS_H
#define BFS_H

#include "structs.h"

/* Use the queue_node.depth variable to hold the allowed depth to be searched from a node */
//int bfs_unvisited( a_star_search_result * asresult, agent_state * as, maps * map);

unsigned long closest_unvisited(maps * map, posn p);

#endif
