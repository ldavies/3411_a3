/*********************************************
 *  constants.h
 *  Agent for Text-Based Adventure Game
 *  Laurence Davies z3342909
 *  COMP3411 Artificial Intelligence
 *  UNSW Session 1, 2013
*/

#ifndef CONSTANTS_H
#define CONSTANTS_H

#define DEBUG

#define MAPSIZE 180 
#define ORIGIN_OFFSET 90 


/* MAP STUFFS */
#define UNVISITED   '.'
#define WHITESPACE  ' '
#define WALL        '*'
#define TREE        'T'
#define WATER       '~'
#define DOOR        '-'
#define KEY         'k'
#define DYNAMITE    'd'
#define AXE         'a'
#define GOLD        'g'

/* AGENT MOVES */
#define FORWARDS    'f'
#define LEFT        'l'
#define RIGHT       'r'
#define CHOP        'c'
#define BLAST       'b'
#define OPEN        'o'

/* PLANNED ACTIONS */
#define ACTION_MOVE_TO           2
#define ACTION_CHOP_AND_MOVE_TO  4
#define ACTION_BLAST_AND_MOVE_TO 8
#define ACTION_OPEN_AND_MOVE_TO  16
#define ACTION_VALID(a) (a & 31 != 0)
#define ACTION_INVALID(a) (a == 0)

//#define AGENT_STATE_INVALID 0
//#define AGENT_STATE_VALID 1

/* SEARCH MAP VALUES */
#define UNBLOCKED   ' '
#define BLOCKED     '#'
#define MAXDEPTHLIMIT 80

#define ALLOW_UNVISITED 1
#define BLOCK_UNVISITED 0

#define MAXQUEUESIZE 10000
//1048575
//2147483648
//32768
//#define MAXBFSITERATIONS 2147483648
#define MAXSEENITEMS 20

#define MAXINVENTORIES 4

#define MAXMOVES 10000
//16384

#define MAXTRAVERSED 150
//2147483648
//262144
//

/* Search types */
#define TARGET_SEARCH_TYPE 1
#define GOLD_SEARCH_TYPE 2
#define UNVISITED_SEARCH_TYPE 3
#define UNVISITED_NODYNAMITE_SEARCH_TYPE 4

#define MAXCOST 2147483648

#endif
