/*********************************************
 *  helpers.c
 *  Sample Agent for Text-Based Adventure Game
 *  COMP3411 Artificial Intelligence
 *  UNSW Session 1, 2013
*/
#include "helpers.h"

/* Math helpers */
int max(int a, int b) {
  return (a > b) ? a : b;
}
int min(int a, int b) {
  return (a < b) ? a : b;
}
int abs(int a) {
  return (a > 0) ? a : -a;
}



