/*********************************************
 *  helpers.h
 *  Agent for Text-Based Adventure Game
 *  Laurence Davies z3342909
 *  COMP3411 Artificial Intelligence
 *  UNSW Session 1, 2013
*/
#ifndef HELPERS_H
#define HELPERS_H

/* Math helpers */
int max(int a, int b);
int min(int a, int b);
int abs(int a);

#endif


