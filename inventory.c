/*********************************************
 *  inventory.c
 *  Agent for Text-Based Adventure Game
 *  Laurence Davies z3342909
 *  COMP3411 Artificial Intelligence
 *  UNSW Session 1, 2013
*/

#include "inventory.h"
#include "item.h"
#include "agent.h"
#include <stdio.h>
#include <stdlib.h>


int init_inventory(inventory * inv) {
  inv->seen = NULL;
  inv->has = 0;
  inv->num_seen = 0;
  inv->num_used = 0;
  inv->heap_next = 0;
  return 1;
}

/* copy inventory function. */
int deep_copy_inventory(inventory * src, inventory * dest) {
  if (!src || !dest) return 0;
  //*dest = *src; /* shallow copy first. TODO copy params only, not heap. */
  dest->type = src->type;
  dest->has = src->has;
  //dest->num_seen = src->num_seen;
  /* heap has been copied, but the seen linked list has the wrong pointers. */
  dest->seen = NULL;
  dest->heap_next=0;
  dest->num_seen = 0;
  dest->num_used = 0;
  /* sight all of the src->seen items */
  item * i = src->seen;
  while (i != NULL) {
    sight(dest, i->p);
    i=i->next;
  }
  return 1;
}

/* inv1 is the queued inventory, inv2 is the comparison.  */
int compare_inventory(inventory * inv1, inventory * inv2) {
  item * i, * j;
  if (!inv1 || !inv2) return 0;
  if
    (
      inv1->has  != inv2->has  ||
      inv1->num_seen != inv2->num_seen ||
      inv1->num_used != inv2->num_used
    ) return 0;
  i = inv1->seen;
  j = inv2->seen;
  while (i && j) {
    if (i->p.i!=j->p.i || i->p.j!=j->p.j) return 0;
    i = i->next; j = j->next;
  }
  return i==NULL && j==NULL; /* final comparison, in case i=NULL or j=NULL exclusively */
}

/* inv1 is the queued inventory, inv2 is the comparison. 
 * We want to compare whether the comparison
 * has *more than* the queued inventory AND has used less.
 * If it has less or has used more, we typically don't want it so return 0.
 * If it is a better inventory, return 1.
 */
int has_inventory_improved(inventory * inv1, inventory * inv2) {
  if (!inv1 || !inv2) return 0;
  if (inv2->has <= inv1->has) return 0;
  //if (inv2->num_used > inv1->num_used) return 0;
  return 1;
}


/* Pushes item onto agent inventory list based on type 
 * Fairly dumb. Doesn't check whether item is already in 
 * inventory at position [i,j] 
 */
/*
int sight(inventory * inv, posn p) {
  item * k = &(inv->heap[inv->heap_next++]);
  *k = init_item(p);
  k->next = NULL;

  if (inv->seen == NULL) {
    inv->seen = k;
    k->previous = NULL;
  }
  else {
    item * i = inv->seen;
    while (i->next != NULL) i = i->next;
    i->next = k;
    k->previous = i;
  }  
  inv->num_seen++;

  return 1;
}

*/


/* Pushes item onto agent inventory list based on type 
 */
int sight(inventory * inv, posn p) {
  if (inv->seen != NULL) {
    item * i = inv->seen;
    while (i->next != NULL) {
      // Check if we have seen an item at this position before
      if (i->p.i == p.i && i->p.j == p.j) return 0;
      i = i->next;
    }
    if (i->p.i == p.i && i->p.j == p.j) return 0;

    item * k = &(inv->heap[inv->heap_next++]);
    *k = init_item(p);

    i->next = k;
    k->previous = i;
  }  
  else {
    item * k = &(inv->heap[inv->heap_next++]);
    *k = init_item(p);
    inv->seen = k;
  }
  inv->num_seen++;

  return 1;
}
