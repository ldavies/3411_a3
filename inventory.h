/*********************************************
 *  inventory.h
 *  Agent for Text-Based Adventure Game
 *  Laurence Davies z3342909
 *  COMP3411 Artificial Intelligence
 *  UNSW Session 1, 2013
*/

#ifndef INVENTORY_H
#define INVENTORY_H

#include "structs.h"

int init_inventory(inventory * inv);
int deep_copy_inventory(inventory * src, inventory * dest);
int compare_inventory(inventory * inv1, inventory * inv2);
int sight(inventory * inv, posn p);

/* inv1 is the queued inventory, inv2 is the comparison. 
 * We want to compare whether the comparison
 * has *more than* the queued inventory AND has used less.
 * If it has less or has used more, we typically don't want it so return 0.
 * If it is a better inventory, return 1.
 */
int has_inventory_improved(inventory * inv1, inventory * inv2);
#endif
