/*********************************************
 *  item.c
 *  Agent for Text-Based Adventure Game
 *  Laurence Davies z3342909
 *  COMP3411 Artificial Intelligence
 *  UNSW Session 1, 2013
*/

#include "item.h"
#include "constants.h"
#include "map.h"

#include <stdio.h>
#include <stdlib.h>


int is_item_type (char m) {
  switch(m) {
    case GOLD:
    case DYNAMITE: 
    case KEY: 
    case AXE: 
     return 1;
  }
  return 0;
}

/* Init with type constant
item init_item(char t, posn p) {
  item r = {.p = p, .previous = NULL, .next = NULL, .type = t};
  return r;
}
*/

/* Init withi posn */
item init_item(posn p) {
  item r = {.p = p, .previous = NULL, .next = NULL};
  return r;
}

int clear_item(item * x) {
    x->previous = NULL;
    x->next = NULL;
    x->p.i = -1;
    x->p.j = -1;
    return 1;
}
