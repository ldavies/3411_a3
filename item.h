/*********************************************
 *  @file item.h
 *  @project Agent for Text-Based Adventure Game
 *  @author Laurence Davies z3342909
 *  @company COMP3411 Artificial Intelligence UNSW
 *  @date Session 1, 2013
 */

#ifndef ITEM_H
#define ITEM_H

#include "structs.h"


int is_item_type (char m);

/* Init with type constant */
//item init_item(char t, posn p);
item init_item(posn p);
int clear_item(item * x);
#endif
