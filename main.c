/*********************************************
 *  main.c
 *  Agent for Text-Based Adventure Game
 *  Laurence Davies
 *  COMP3411 Artificial Intelligence
 *  UNSW Session 1, 2013
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "structs.h"
#include "pipe.h"
#include "constants.h"
#include "helpers.h"
#include "item.h"
#include "inventory.h"
#include "map.h"
#include "agent.h"
#include "astar.h"
#include "bfs.h"
#include "view.h"

/* Network globals */
int   pipe_fd;
FILE* in_stream;
FILE* out_stream;


/*
 * The priority is to take the gold if location is known and agent can reach it.
 *  
 * #Heuristics
 * 
 * Manhattan Distance to target is the admissible heuristic for gold/dynamite/key/axe
 * A modified BFS is used for uncovering the map. Previously searched map area
 * imparts a cost penalty, as does the lack of inventory items.
 * 
 * The function check_agent_state() is used globally for path checking
 *   - A traversable cell is
 *        whitespace ( ), 
 *        tree (T) iff agent has axe, 
 *        door (-) iff agent has key
 *        wall (*) iff agent has dynamite AND justification
 *   
 * The A* search algorithm is used to find the shortest possible path
 * to a target. 
 * To be sparing with the dynamite, an additional cost is added to paths
 * that require use of it.
 * 
 * Currently this agent does not have the capability of dealing with uncertainty.
 */


int decide_moves( a_star_search_result * asresult, agent_state * agent , maps * map) {
  if (!asresult) return 0;
  asresult->found = 0; // init.

  inventory * gold     = get_inventory(agent,GOLD);

  set_map_extents(map,agent->p);

  debug_print_result(asresult,"after set extents",agent,map);

  /* If the agent has the gold then return to the origin */
  if (gold->has) {
    int success = a_star_origin(asresult, agent, map);
  }
  else {
    /* zero inventory change unvisited search */
    int success = a_star_unvisited_nodynamite(asresult, agent, map);
  }
  /* If the gold has been located, it is the target */
  /* Go for the closest gold */
  if (!asresult->found && gold->seen != NULL) {
    int success = a_star_gold(asresult, agent, map);
  }
  /* If all else fails, search for unvisited space. */
  if (!asresult->found) {
#ifdef DEBUG
    printf("Start unvisited search\n");
#endif
    //int success = bfs_unvisited(asresult, agent, map);
    int success = a_star_unvisited(asresult, agent, map);
    if (success==0) {
      printf("Failed unvisited search.\n");
    }
    else if (success==-1) {
      printf("Unvisited search queue size exceeded.\n");
    }
  }

  //if(asresult->found || sresult.found) {
  if(asresult->found) {
    return 1;
  }
  
  printf("Could not find a move\n");
  exit(0); // we should never reach here.
  return 0;
}



int main( int argc, char *argv[] )
{
  a_star_search_result  * a = malloc(sizeof(a_star_search_result));
  int cur_move = 0;

  //char action;
  int sd;
  int ch;
  int i,j;

  /* View buffer */
  char view[5][5];
  maps  map;
  map.extent_i_min = ORIGIN_OFFSET;
  map.extent_i_max = ORIGIN_OFFSET;
  map.extent_j_min = ORIGIN_OFFSET;
  map.extent_j_max = ORIGIN_OFFSET;

  /* First declaration of the variable agent */
  agent_state agent;
  init_agent (&agent);


  if ( argc < 3 ) {
    printf("Usage: %s -p port\n", argv[0] );
    exit(1);
  }

    // open socket to Game Engine
  sd = tcpopen("localhost", atoi( argv[2] ));

  pipe_fd    = sd;
  in_stream  = fdopen(sd, "r");
  out_stream = fdopen(sd, "w");

  // init the visible map we are creating
  init_map(map.visible, '.');

  while(1) {
    // scan 5-by-5 wintow around current location
    printf("scan view\n" );
    for( i=0; i < 5; i++ ) {
      for( j=0; j < 5; j++ ) {
        if( !(( i == 2 )&&( j == 2 ))) {
          ch = getc( in_stream );
          if( ch == -1 ) {
            exit(1);
          }
          view[i][j] = ch;
        }
      }
    }

    printf("copy to map\n" );
    copy_view_to_map( view , &map, &agent);
#ifdef DEBUG
    print_view( view, &map, &agent); // COMMENT THIS OUT BEFORE SUBMISSION
#endif
    if (cur_move == a->length) {
      decide_moves( a, &agent, &map );
      cur_move = 0;
    }

    duplicate_agent(&a->moves[cur_move++],&agent);

    putc( agent.move, out_stream );
    fflush( out_stream );
  }

  return 0;
}
