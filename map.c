/*********************************************
 *  map.c
 *  Agent for Text-Based Adventure Game
 *  Laurence Davies z3342909
 *  COMP3411 Artificial Intelligence
 *  UNSW Session 1, 2013
*/

#include "map.h"
#include "constants.h"
#include "helpers.h"


void set_map_extents(maps * m, posn p) {
  m->extent_i_min = min(p.i - 3,m->extent_i_min);
  m->extent_i_max = max(p.i + 3,m->extent_i_max);
  m->extent_j_min = min(p.j - 3,m->extent_j_min);
  m->extent_j_max = max(p.j + 3,m->extent_j_max);
}

void init_map(char m[MAPSIZE][MAPSIZE], char value) {

  int i,j;
  for (i=0;i<MAPSIZE;i++)
    for (j=0;j<MAPSIZE;j++)
      m[i][j]=value;

}


int init_cost_path(maps * m) {
  int i,j;
  for (i=0;i<MAPSIZE;i++)
    for (j=0;j<MAPSIZE;j++)
      m->cost_path[i][j]=MAXCOST;

  return 1;
}

/* returns 1 if cost < m->cost_path[i][j], 0 otherwise */
int compare_cost_path(posn p, maps * m, int cost) {
  return cost < m->cost_path[p.i][p.j];
}

int set_cost_path(posn p, maps * m, int cost) {
  m->cost_path[p.i][p.j] = cost;
  return 1;
}

int clear_cost_path(posn p, maps * m) {
  m->cost_path[p.i][p.j] = 0;
  return 1;
}
/*
int init_search_path(maps * m) {
  init_map(m->search_path, UNBLOCKED);
  return 1;
}

int init_persistent_blocks(maps * m) {
  init_map(m->persistent_block, UNBLOCKED);
  return 1;
}

int block_path(posn p, maps * m) {
  m->search_path[p.i][p.j] = BLOCKED;
  return 1;
}

int unblock_path(posn p, maps * m) {
  m->search_path[p.i][p.j] = UNBLOCKED;
  return 1;
}

int block_persistent(posn p, maps * m) {
  m->persistent_block[p.i][p.j] = BLOCKED;
  return 1;
}

int unblock_persistent(posn p, maps * m) {
  m->persistent_block[p.i][p.j] = UNBLOCKED;
  return 1;
}

// checks both the search path map and the persistent route map 
int is_unblocked(posn p, maps * m) {
  return (m->search_path[p.i][p.j] == UNBLOCKED && m->persistent_block[p.i][p.j] == UNBLOCKED);
}
*/

/* moves defines possible movements
   The first index is the direction the agent is facing, 
   starting at 0 = down the screen.
   The second index is the axis.
   Turning left rotates ccw so adds -1 to facing
   Turning right rotates cw so adds +1 to facing
 */
const int moves[4][2] = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};

int get_move(int r, int i) {
  return moves[r][i];
}

posn move_posn(posn p, int r) {
  p.i += moves[r][0];
  p.j += moves[r][1];
  return p;
}

posn offset_posn(posn p, int oi, int oj) {
  p.i += oi;
  p.j += oj;
  return p;
}

posn init_posn(int i, int j) {
  posn p = {.i=i, .j=j};
  return p;
}


/* Keep a tally of the amount of uncovered space. Init to 1 to account for agent space. */
int uncovered_space = 1;

int increment_uncovered() {
  uncovered_space++;
  return 1;
}
int get_uncovered() {
  return min(uncovered_space,MAXDEPTHLIMIT);
}

int viewing_window_has_unvisited(maps * m, posn p) {
  int i,j;
  for (i=-2;i<=2;i++)
    for (j=-2;j<=2;j++)
      if (map_visible(m,offset_posn(p,i,j)) == UNVISITED) return 1;

  return 0;
}

char map_visible(maps * m, posn p) {
    return m->visible[p.i][p.j];
}

int set_visible(maps * m, posn p, char c) {
  m->visible[p.i][p.j] = c;
  return 1;
}
