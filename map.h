/*********************************************
 *  map.c
 *  Agent for Text-Based Adventure Game
 *  Laurence Davies z3342909
 *  COMP3411 Artificial Intelligence
 *  UNSW Session 1, 2013
*/

#ifndef MAP_H
#define MAP_H

#include "structs.h"

void set_map_extents(maps * m,posn p);

void init_map(char m[MAPSIZE][MAPSIZE], char value);

int init_cost_path(maps * m);

int compare_cost_path(posn p, maps * mi, int cost);

int set_cost_path(posn p, maps * m, int cost);
/*
int init_search_path(maps * m);

int init_persistent_blocks(maps * m);

int block_path(posn p, maps * m);

int unblock_path(posn p, maps * m);

int block_persistent(posn p, maps * m);

int unblock_persistent(posn p, maps * m);

// checks both the search path map and the persistent route map 
int is_unblocked(posn p, maps * m);
*/

int get_move(int r, int i);
posn move_posn(posn p, int r);
posn offset_posn(posn p, int oi, int oj);
posn init_posn(int i, int j);

int set_visible(maps * m, posn p, char c);

int increment_uncovered();
int get_uncovered();

int viewing_window_has_unvisited(maps * m, posn p);
char map_visible(maps * m, posn p);

#endif
