/*********************************************
 *  @file structs.h
 *  @project Text-Based Adventure Game
 *  @author Laurence Davies z3342909
 *  @company COMP3411 Artificial Intelligence UNSW
 *  @date Session 1, 2013
 */

#ifndef STRUCTS_H
#define STRUCTS_H

#include "constants.h"

/* map */
typedef struct {
  int i;
  int j;
} posn;


/* Map and blockout for searches */
typedef struct {
  char visible          [MAPSIZE][MAPSIZE];
//  char search_path      [MAPSIZE][MAPSIZE];
//  char persistent_block [MAPSIZE][MAPSIZE];
  unsigned long cost_path        [MAPSIZE][MAPSIZE];
  /* Visited extents */
  int  extent_i_min;
  int  extent_i_max;
  int  extent_j_min;
  int  extent_j_max;
} maps;

/* item */
typedef struct item {
  posn          p;
  struct item * previous;
  struct item * next;
} item;

/* inventory */
typedef struct inventory {
  int     type;
  int     has;        /* number of items contained within this inventory */
  item *  seen;
  int     num_seen;
  int     num_used;
  item    heap[MAXSEENITEMS];
  int     heap_next;  /* Next heap index */
} inventory;

/* agent */

typedef struct {
  posn      p;
  int       r;
  inventory inv[MAXINVENTORIES];
  char      move;
  maps      map;
  posn      traversed[MAXTRAVERSED]; // Any map position that has been traversed will be blank
  int       traversed_length;
} agent_state;

typedef struct {
  int length;   // length of path IF FOUND
  unsigned long cost;     // f(x) + g(x) i.e. depth traversed plus cost heuristic to goal.
  int found;    // sentinel - did this path find the goal
  posn p;       // location of target
  agent_state moves[MAXMOVES];
} a_star_search_result;


/* A star queue */

typedef struct queue_node {
  agent_state as;
  unsigned long cost;
  int depth;
  char action;
  struct queue_node * previous_move;
  struct queue_node * next_move;
  struct queue_node * parent;
  struct queue_node * child; 
} queue_node;

/* Avoid OTF mallocs by allocating a heap in the queue struct. */
typedef struct queue {
  queue_node   heap[MAXQUEUESIZE];
  queue_node * first_open;
  queue_node * first_closed;
  int          heap_next;
} queue;

#endif
