/*********************************************
 *  view.c
 *  Agent for Text-Based Adventure Game
 *  Laurence Davies z3342909
 *  COMP3411 Artificial Intelligence
 *  UNSW Session 1, 2013
*/

#include "view.h"
#include "item.h"
#include "agent.h"
#include "map.h"

#include <stdio.h>
#include <stdlib.h>


void copy_view_to_map(char view[5][5],maps * map,agent_state * a) {
 
  int i,j;
  int mi=0,mj=0;
  
  for( i=0; i < 5; i++ ) {
    for( j=0; j < 5; j++ ) {
      if( (( i == 2 )&&( j == 2 ))) {
        map->visible[a->p.i][a->p.j] = WHITESPACE; /* Overwrite anything under the agent */
      } else {
        /* Transform the viewport */
        switch(a->r) {
          case 0: // facing +i
            mi= i-2;
            mj= j-2;break;
          case 1: // facing -j
            mi= j-2;
            mj= -(i-2);break;
          case 2: // facing -i
            mi= -(i-2);
            mj= -(j-2);break;
          case 3: // facing +j
            mi= -(j-2);
            mj= i-2;break;
        }
        //mi += a->p.i;
        //mj += a->p.j;
        posn map_pos = offset_posn(a->p,mi,mj);


        /* Have we uncovered previously unidentified space? */
        //if (map->visible[mi][mj] == UNVISITED) {
        if (map_visible(map,map_pos) == UNVISITED) {

          increment_uncovered();

          /* Identify items */
          //posn p = {.i = mi, .j = mj};
          if (is_item_type(view[i][j])) agent_sight(a,view[i][j],map_pos);
          //else (is_traversable(view[i][j])) increment_traversable();

        }
        /* reference the map and copy over the view */
        //map->visible[mi][mj] = view[i][j];
        set_visible(map,map_pos,view[i][j]);
      }
    }
  }

}


void print_view(char view[5][5], maps * map, agent_state * agent)
{
  int i,j;

  printf("\n+-----+\n");
  for( i=0; i < 5; i++ ) {
    putchar('|');
    for( j=0; j < 5; j++ ) {
      if(( i == 2 )&&( j == 2 )) {
        putchar( '^' );
      }
      else {
        putchar( view[i][j] );
      }
    }
    printf("|\n");
  }
  printf("+-----+\n");
  printf("\n");

  printf("Move: %c\n\n",agent->move);

  /* Now print the whole map */
  print_map(map,agent);
}



int print_map(maps * m, agent_state * agent) {
  
  int i,j;
  int i_min = m->extent_i_min;
  int i_max = m->extent_i_max;
  int j_min = m->extent_j_min;
  int j_max = m->extent_j_max;
  //for( i=3*MAPSIZE/9; i < 5*MAPSIZE/8; i++ ) {
  for( i=i_min; i <= i_max; i++ ) {
    for( j=j_min; j <= j_max; j++ ) {
    //for( j=0; j < MAPSIZE; j++ ) {
      if(( i == agent->p.i )&&( j == agent->p.j )) {
        switch(agent->r) {
          case 0:
            putchar( '^' );break;
          case 1:
            putchar( '>' );break;
          case 2:
            putchar( 'v' );break;
          case 3:
            putchar( '<' );break;
          }
      }
      else if (m->visible[i][j] != 0) {
        putchar( m->visible[i][j] );
      }
      else {
        putchar( UNVISITED );
      }
    }
    printf("\n");
  }
  printf("\n");

  /* debug: print agent positionand orientation */
  printf("i = %d j = %d r = %d\n\n",
         agent->p.i,
         agent->p.j,
         agent->r);
  
  return 1;
}


int print_cost_map(maps * m, agent_state * as) {
  
  int i,j;
  int i_min = m->extent_i_min;
  int i_max = m->extent_i_max;
  int j_min = m->extent_j_min;
  int j_max = m->extent_j_max;
  for( i=i_min; i <= i_max; i++ ) {
    for( j=j_min; j <= j_max; j++ ) {
      if (i==as->p.i && j==as->p.j) {
        switch(as->r) {
          case 0:
            putchar( '^' );break;
          case 1:
            putchar( '>' );break;
          case 2:
            putchar( 'v' );break;
          case 3:
            putchar( '<' );break;
          }
      }
      else putchar( '0' + (m->cost_path[i][j] % 78) );
    }
    printf("\n");
  }

  // print backspaces
  for( i=i_min; i <= i_max; i++ ) {
    printf("\x1B[A");
  }
  
  return 1;
}  


int reset_cost_map_view(maps * m) {
  int i;
  int i_min = m->extent_i_min;
  int i_max = m->extent_i_max;
  for( i=i_min; i <= i_max; i++ ) {
    printf("\n");
  }
  return 1;
}
