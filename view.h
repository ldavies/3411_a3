/*********************************************
 *  view.h
 *  Agent for Text-Based Adventure Game
 *  Laurence Davies z3342909
 *  COMP3411 Artificial Intelligence
 *  UNSW Session 1, 2013
*/

#ifndef VIEW_H
#define VIEW_H

#include "structs.h"

void copy_view_to_map(char view[5][5], maps * map, agent_state * a);

void print_view(char view[5][5],maps * map, agent_state * agent);

int print_map(maps * m, agent_state * agent);
int print_cost_map(maps * m, agent_state *as);
int reset_cost_map_view(maps * m);
#endif
